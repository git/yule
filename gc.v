`include "gcram.v"

module GC (input clk, input clk_enable, input [15:0] Ein, output [15:0] Eout, input [3:0] gcop, output [5:0] ostate, output conn_et, output conn_ea, output step_eval, output ram_we, output [12:0] ram_addr, output [15:0] ram_di, input [15:0] ram_do, output [12:0] freeptr);
   reg [15:0] rom_output;
   reg [5:0]  gostate;
   reg [5:0]  gnstate;

`ifdef SIM
   initial begin
	  gostate <= 0;
   end
`endif

   wire ga_zero_disp = rom_output[15];
   wire gcop_disp    = rom_output[14];
   assign ram_we     = rom_output[13];
   wire adr          = rom_output[12];
   wire rdR          = rom_output[11];
   wire rdQ          = rom_output[10];
   wire rdP_plus     = rom_output[9];
   wire rdP          = rom_output[8];
   wire ldS          = rom_output[7];
   wire ldR          = rom_output[6];
   wire ldQ          = rom_output[5];
   wire ldP          = rom_output[4];
   wire conn_i       = rom_output[3];
   assign conn_et    = rom_output[2];
   assign conn_ea    = rom_output[1];
   assign step_eval  = rom_output[0];

   wire ga_zero = ~|G[12:0];

   always @* begin
	  case(gostate)
		6'o00:   begin rom_output <= 16'o010242; gnstate <= 6'o01; end
		6'o01:   begin rom_output <= 16'o000031; gnstate <= 6'o02; end
		6'o02:   begin rom_output <= 16'o040000; gnstate <= 6'o20; end
		6'o03:   begin rom_output <= 16'o000126; gnstate <= 6'o04; end
		6'o04:   begin rom_output <= 16'o001200; gnstate <= 6'o05; end
		6'o05:   begin rom_output <= 16'o002020 | (1 << 12); gnstate <= 6'o06; end
		6'o06:   begin rom_output <= 16'o000051; gnstate <= 6'o02; end
		6'o07:   begin rom_output <= 16'o002020; gnstate <= 6'o10; end
		6'o10:   begin rom_output <= 16'o001200; gnstate <= 6'o11; end
		6'o11:   begin rom_output <= 16'o004020; gnstate <= 6'o12; end
		6'o12:   begin rom_output <= 16'o002100 | (1 << 12); gnstate <= 6'o13; end
		6'o13:   begin rom_output <= 16'o000057; gnstate <= 6'o02; end
		6'o14:   begin rom_output <= 16'o004020; gnstate <= 6'o04; end
		6'o15:   begin rom_output <= 16'o000246; gnstate <= 6'o16; end
		6'o16:   begin rom_output <= 16'o020001; gnstate <= 6'o02; end
		6'o17:   begin rom_output <= 16'o002100; gnstate <= 6'o42; end
		6'o20:   begin rom_output <= 16'o000001; gnstate <= 6'o02; end
		6'o21:   begin rom_output <= 16'o010306; gnstate <= 6'o06; end
		6'o22:   begin rom_output <= 16'o000440; gnstate <= 6'o03; end
		6'o23:   begin rom_output <= 16'o012200; gnstate <= 6'o12; end
		6'o24:   begin rom_output <= 16'o000500; gnstate <= 6'o07; end
		6'o25:   begin rom_output <= 16'o004040; gnstate <= 6'o24; end
		6'o26:   begin rom_output <= 16'o014200; gnstate <= 6'o06; end
		6'o27:   begin rom_output <= 16'o000440; gnstate <= 6'o14; end
		6'o30:   begin rom_output <= 16'o012300; gnstate <= 6'o06; end
		6'o31:   begin rom_output <= 16'o111300; gnstate <= 6'o44; end
		6'o32:   begin rom_output <= 16'o111300; gnstate <= 6'o40; end
		6'o33:   begin rom_output <= 16'o014200; gnstate <= 6'o15; end
		6'o34:   begin rom_output <= 16'o000047; gnstate <= 6'o02; end
		6'o35:   begin rom_output <= 16'o002007; gnstate <= 6'o02; end
		6'o36:   begin rom_output <= 16'o002003; gnstate <= 6'o02; end
		6'o37:   begin rom_output <= 16'o014200; gnstate <= 6'o55; end
		6'o40:   begin rom_output <= 16'o004020; gnstate <= 6'o17; end
		6'o41:   begin rom_output <= 16'o000000; gnstate <= 6'o41; end
		6'o42:   begin rom_output <= 16'o000206; gnstate <= 6'o47; end
		6'o43:   begin rom_output <= 16'o000106; gnstate <= 6'o46; end
		6'o44:   begin rom_output <= 16'o004020; gnstate <= 6'o43; end
		6'o45:   begin rom_output <= 16'o000000; gnstate <= 6'o41; end
		6'o46:   begin rom_output <= 16'o002200; gnstate <= 6'o47; end
		6'o47:   begin rom_output <= 16'o020440; gnstate <= 6'o50; end
		6'o50:   begin rom_output <= 16'o111200; gnstate <= 6'o52; end
		6'o51:   begin rom_output <= 16'o021100; gnstate <= 6'o54; end
		6'o52:   begin rom_output <= 16'o004200; gnstate <= 6'o51; end
		6'o53:   begin rom_output <= 16'o000000; gnstate <= 6'o41; end
		6'o54:   begin rom_output <= 16'o004021; gnstate <= 6'o02; end
		6'o55:   begin rom_output <= 16'o002100; gnstate <= 6'o56; end
		6'o56:   begin rom_output <= 16'o000050; gnstate <= 6'o57; end
		6'o57:   begin rom_output <= 16'o004007; gnstate <= 6'o02; end
		default: begin rom_output <= 16'o040000; gnstate <= 6'o20; end
	  endcase; // case (gostate)
   end // always @ *

   always @ (posedge clk) begin
	  if(clk_enable)
		gostate <=
				  ga_zero_disp ? (gnstate | ga_zero) :
				  gcop_disp ? (gnstate | gcop) :
				  gnstate;
	  else
		gostate <= 0;
   end // always @ (posedge clk)

   assign ostate = gostate;

   reg [12:0] P;
   reg [15:0] Q;
   reg [15:0] R;
   reg [15:0] S;

   reg [12:0] A; // latched address

   assign freeptr = P;
   assign ram_addr = A;
   assign ram_di = S;

   wire [15:0] G;
   wire [15:0] Gout;

   wire [15:0] GfromR = rdR ? R : 0;
   wire [15:0] GfromQ = rdQ ? Q : 0;
   wire [15:0] GfromP = rdP ? {3'b0, P} : 0;
   wire [15:0] GfromP_plus = rdP_plus ? {3'b0, P + 1} : 0;
   wire [15:0] GfromI = conn_i ? ram_do : 0;
   wire [12:0] GAfromE = conn_ea ? Ein[12:0] : 0;
   wire [2:0] GTfromE = conn_et ? Ein[15:13] : 0;
   wire [15:0] GfromE = {GTfromE, GAfromE};

   assign G = GfromR | GfromQ | GfromP | GfromP_plus | GfromI | GfromE;
   assign Gout = GfromR | GfromQ | GfromP | GfromP_plus | GfromI;

   assign Eout[12:0]  = conn_ea ? Gout[12:0]  : 0;
   assign Eout[15:13] = conn_et ? Gout[15:13] : 0;

   always @ (posedge clk) begin
	  if(clk_enable) begin
		 if (ldS) S = G;
		 if (ldP) P <= G[12:0];
		 if (ldR) R <= G;
		 if (ldQ) Q <= G;
		 if (adr) A <= S[12:0];
	  end
   end
endmodule // GC

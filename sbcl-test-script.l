(defmacro named-lambda (name args &body body)
  `(labels ((,name ,args ,@body))
     #',name))

(defun reverse-list (&rest args)
  (reverse args))

(defun fake-repl ()
  (do ((+eof+ (gensym)))
      (nil)
	(setf - (read *standard-input* nil +eof+))
	(when (eq - +eof+) (return-from fake-repl))
	(format t "~A~%" (eval -))))

(setq *print-pretty* 'nil)
(fake-repl)

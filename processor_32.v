`include "ram.v"
`include "prescaler.v"
`include "single_trigger.v"
`include "multiple_trigger.v"

`define INST_JMP           3'b000
`define INST_JPR           3'b001
`define INST_LDN           3'b010
`define INST_STO           3'b011
`define INST_SUB           3'b100
`define INST_SKN           3'b110
`define INST_HALT          3'b111

`define PC_INC_BUTTON      buttons[8]
`define PC_DEC_BUTTON      buttons[9]
`define PAGE_INC_BUTTON    buttons[10]
`define PAGE_DEC_BUTTON    buttons[11]
`define PC_CLR_BUTTON      buttons[12]
`define ACCUM_CLR_BUTTON   buttons[13]
`define RUN_BUTTON         buttons[14]
`define EXECUTE_BUTTON     buttons[15] 

function [7:0] reverse (input [7:0] forward);
   integer i;
   for (i = 0; i < 8; i = i + 1)
     reverse[7-i] = forward[i];
endfunction

function [7:0] select (input condition, input [7:0] word);
   integer i;
   select = condition ? word : 8'b00000000;
endfunction
  
// This is a thirty-two bit accumulator machine identical to the Manchester SSEM.

module PROCESSOR (input clk, output [23:0] led, output [3:0] indicators, input [15:0] buttons);

    // We use two clocks - the main one for all the registers and one for the RAM
    // The RAM clock is twice as fast as the register one to simulate the effect
    // of flow through RAM which is not supported on the FPGA

    wire clock;

    PRESCALER #(.BITS(2)) scal0 (.clk(clk), .out(clock));
    
    // Handle running
      
    reg running = 0;

    always @ (posedge `RUN_BUTTON) begin
    
        running <= !running;
    
    end

    // Generate running clock

    wire running_counter;
   
    PRESCALER #(.BITS(6)) scal1 (.clk(clock), .out(running_counter));
   
    wire running_clk = running & running_counter;
   
    // Handle execution

    wire [4:0] execute_trigger;
   
    MULTIPLE_TRIGGER #(.BITS(4)) trig0 (.clk(clock), .trigger_in(!running & `EXECUTE_BUTTON), .trigger_out(execute_trigger));

    wire [4:0] running_trigger;

    MULTIPLE_TRIGGER #(.BITS(5)) trig1 (.clk(clock), .trigger_in(running_clk), .trigger_out(running_trigger));

    wire [4:0] execute = execute_trigger | running_trigger;

    // Handle halt

    reg halt = 0;

    wire newHalt = execute[3] & (inst == `INST_HALT) ? 1 : 
	                 !running ? 0 : 
	                 halt;

    always @ (posedge clock) begin

       halt <= newHalt;
    
    end
   
    // Handle program space

    // Note that this uses the RAM clock even for the buffer update. Failing to do results in two buffer updates
    // which leaves the RAM contents unchanged.
   
    wire [31:0] pOut;

    wire [7:0] reverseButtons = reverse(buttons[7:0]);
  
    wire [31:0] buffer = { select(page == 2'b11, reverseButtons), select(page == 2'b10, reverseButtons), select(page == 2'b01, reverseButtons), select(page == 2'b00, reverseButtons) } ^ pOut;
 
    wire write_trigger;
   
    wire program_buttons = (buttons[0] | buttons[1] | buttons[2] | buttons[3] | buttons[4] | buttons[5] | buttons[6] | buttons[7]);
   
    SINGLE_TRIGGER trig2 (.clk(clk), .trigger_in(program_buttons), .trigger_out(write_trigger));

    wire [4:0] address = (execute[2] | execute[3]) ? addr : pc;

    wire [31:0] pIn = execute[2] & (inst == `INST_STO) ? accum : buffer;

    wire write_enable = (!running & write_trigger) | (execute[2] & (inst == `INST_STO));
	     
    RAM #(.DATA_BITS(32),.ADDRESS_BITS(5)) programMemory (.clk(clk), .write(write_enable), .addr(address), .in_data(pIn), .out_data(pOut));
 		
    // Handle page
   
    reg [1:0] page = 0;

    wire page_prev_trigger;
    wire page_next_trigger;
   
    SINGLE_TRIGGER trig7 (.clk(clock), .trigger_in(`PAGE_DEC_BUTTON), .trigger_out(page_prev_trigger));
    SINGLE_TRIGGER trig8 (.clk(clock), .trigger_in(`PAGE_INC_BUTTON), .trigger_out(page_next_trigger));

    wire [1:0] newPage = page_prev_trigger ? (page + 2'b11) : page_next_trigger ? (page + 2'b01) : page;

    always @ (posedge clock) begin

       page <= newPage;

    end
   
    // Handle PC

    reg [4:0] pc = 0;
   
    wire [4:0] nextPc = pc + 5'b00001;
    wire [4:0] prevPc = pc + 5'b11111;

    wire pc_prev_trigger;
    wire pc_next_trigger;
    wire pc_zero_trigger;
   
    SINGLE_TRIGGER trig3 (.clk(clock), .trigger_in(`PC_INC_BUTTON), .trigger_out(pc_next_trigger));
    SINGLE_TRIGGER trig4 (.clk(clock), .trigger_in(`PC_DEC_BUTTON), .trigger_out(pc_prev_trigger));
    SINGLE_TRIGGER trig5 (.clk(clock), .trigger_in(`PC_CLR_BUTTON), .trigger_out(pc_zero_trigger));

    wire [4:0] newPc = execute[3] & (inst == `INST_JMP) ? pOut[4:0] :
	                     execute[3] & (inst == `INST_JPR) ? pc + pOut[4:0] :
	                     execute[3] & (inst == `INST_SKN) & (accum[31] == 1) ? nextPc :
		                   !halt & execute[0] ? nextPc :
	                     !running & pc_zero_trigger ? 5'b00000 : 
	                     !running & pc_next_trigger ? nextPc : 
	                     !running & pc_prev_trigger ? prevPc : 
	                     pc;
   
    always @ (posedge clock) begin

       pc <= newPc;
       
    end

    // Handle instruction
			  
    reg [2:0] inst = 0;
    reg [4:0] addr = 0;

    wire [2:0] newInst = execute[1] ? pOut[15:13] :
                         inst;

    wire [4:0] newAddr = execute[1] ? pOut[4:0] :
                 	       addr;
   
    always @ (posedge clock) begin

       inst <= newInst;

       addr <= newAddr;
			  
    end
			  
    // Handle accumulator

    reg [31:0] accum = 0;

    wire accum_zero_trigger;

    SINGLE_TRIGGER trig6 (.clk(clock), .trigger_in(`ACCUM_CLR_BUTTON), .trigger_out(accum_zero_trigger));

    wire [31:0] newAccum = execute[2] & (inst == `INST_LDN) ? 0 - pOut :
	                         execute[2] & (inst == `INST_SUB) ? accum - pOut :
		                       !running & accum_zero_trigger ? 0 :
	                         accum;
   
    always @ (posedge clock) begin

      accum <= newAccum;
       
    end
  
    // Assign the outputs

    wire [7:0] selectedOut = (page == 2'b00) ? pOut[7:0] : 
 	                           (page == 2'b01) ? pOut[15:8] :
  	                         (page == 2'b10) ? pOut[23:16] :
	                           pOut[31:24];

    wire [7:0] selectedAccum = (page == 2'b00) ? accum[7:0] : 
	                             (page == 2'b01) ? accum[15:8] :
	                             (page == 2'b10) ? accum[23:16] :
	                             accum[31:24];
   
   assign led[7:0] = reverse(selectedOut);
   assign led[15:8] = reverse(selectedAccum);
   assign led[23:16] = {page, 1'b0, pc};
   assign indicators = {1'b0, (!running & `EXECUTE_BUTTON) | running_clk, halt, running & !halt};

endmodule


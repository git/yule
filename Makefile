PROJ = lisp_processor
PIN_DEF = lisp_processor.pcf
DEVICE = hx1k

all: $(PROJ).rpt $(PROJ).bin

%.blif: %.v
	tools/yosys/yosys -p 'synth_ice40 -top cpu -blif $@' $<

%.asc: $(PIN_DEF) %.blif
	tools/arachne-pnr/bin/arachne-pnr -d $(subst hx,,$(subst lp,,$(DEVICE))) -o $@ -p $^ -P tq144

%.bin: %.asc
	tools/icestorm/icepack/icepack $< $@

%.rpt: %.asc
	tools/icestorm/icetime/icetime -C tools/icestorm/icebox/chipdb-$(subst hx,,$(subst lp,,$(DEVICE))).txt -d $(DEVICE) -mtr $@ $<

prog: $(PROJ).bin
	tools/icestorm/iceprog/iceprog $<

clean:
	rm -f $(PROJ).blif $(PROJ).asc $(PROJ).bin

.PHONY: all prog clean

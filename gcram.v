module GCRAM
(input clk, input we, input[12:0] addr, input[15:0] di, output reg [15:0] do);
   reg [15:0] mem [4095:0];

   always @ (negedge clk)
	 do <= #1 mem[addr];

   always @ (negedge clk)
	 if (we)
	   mem[addr] <= #1 di;

   initial begin
	  mem[ 0] <= 0;                     // (cdr part of NIL)
	  mem[ 1] <= 0;                     // (car part of NIL)
	  mem[ 2] <= 16'b0010000000000000;  // (cdr part of T)
	  mem[ 3] <= 16'b0010000000000000;  // (car part of T)
	  mem[ 4] <= 16'd12;                // (free storage pointer)
	  mem[ 5] <= 16'b1100000000000111;  // CALL 7
	  mem[ 6] <= 0;                     // (result of computation)
	  mem[ 7] <= 16'b0000000000001001;  // MORE 9
	  mem[ 8] <= 16'b0010000000000101;  // NUMBER 5
	  mem[ 9] <= 16'b1110000000000000;  // FUNCALL NIL
	  mem[10] <= 16'b1000000000001011;  // PROC 11
	  mem[11] <= 16'b0101111111111110;  // VAR -2
   end
endmodule

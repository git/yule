(defmacro named-lambda (name args &body body)
  `(labels ((,name ,args ,@body))
     #',name))

(defun reverse-list (&rest args)
  (reverse args))

